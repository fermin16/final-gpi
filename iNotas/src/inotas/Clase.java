/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inotas;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Miguel Colomo
 */
public class Clase implements Serializable {

    Profesor encargado;
    ArrayList<Profesor> profesor;
    ArrayList<Alumno> listaAlumnos;
    HashMap<Alumno, EvaPruebas> notas;
    Evaluacion planClase;
    String nombreClase;

    /**
     * Constructor de clase.
     *
     * @param encargado
     * @param nombre
     * @param plan
     */
    public Clase(Profesor encargado, String nombre, Evaluacion plan) {
        this.encargado = encargado;
        listaAlumnos = new ArrayList<>();
        nombreClase = nombre;
        planClase = plan;
        profesor = new ArrayList<>();
    }

    /**
     *
     * @param profe
     */
    public void añadeProfesor(Profesor profe) {
        profesor.add(profe);
    }

    public void añadeAlumnos(ArrayList<Alumno> lal) {
        listaAlumnos.addAll(lal);
    }

    public void eliminaProfesor(Profesor profe) {
        Iterator<Profesor> it = profesor.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (((Profesor) it).getDNI() == profe.getDNI()) {
                profesor.remove(i);
                break;
            }
            i++;
        }
    }

    public void eliminaAlumno(Alumno al) {
        Iterator<Alumno> it = listaAlumnos.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (((Alumno) it).getNIA() == al.getNIA()) {
                listaAlumnos.remove(i);
                break;
            }
            i++;
        }
    }

    public void modificaEvaluacion(int nApartados, String[] nombres, ApartadoEvaluacion[] apartados) {
        planClase.modificarEvluacion(nApartados, nombres, apartados);
    }

    public void calificarExamen(Alumno alumno, String examen, double nota) {
        EvaPruebas nuevo = notas.get(alumno);
        nuevo.añadirNota(examen, nota);
    }

    public void calcularNotasFinales() {
        for (EvaPruebas caso : notas.values()) {
            double nota=0;
            for(ApartadoEvaluacion apartado: planClase.getEvaluacion().values()){
                double notaParcial=0;
                for ()        
            }
            caso.setNotaFinal(nota);
        }
    }
}
