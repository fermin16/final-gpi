/*
 * Copyright (C) 2019 mintxo laptop pro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package inotas;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author mintxo laptop pro
 */
class ApartadoEvaluacion implements Serializable{

    private HashMap<String, Integer> apartados;

    public ApartadoEvaluacion(int nApartados, String[] nombres, int[] porcentajes) {
        apartados = new HashMap<>();
        if (nApartados >= 1 && nombres.length == nApartados && porcentajes.length == nApartados) {
            for (int i = 0; i < nApartados; i++) {
                apartados.put(nombres[i], porcentajes[i]);
            }
        }
    }
    
    public void modicarApartadoEvaluacion(int nApartados, String[] nombres, int[] porcentajes){
        apartados.clear();
        if (nApartados >= 1 && nombres.length == nApartados && porcentajes.length == nApartados) {
            for (int i = 0; i < nApartados; i++) {
                apartados.put(nombres[i], porcentajes[i]);
            }
        }
    }

}
